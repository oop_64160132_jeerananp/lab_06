package com.jeeranan132.week6;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class BookbankTest {
    @Test
    public void shouldDepositSuccess() {
        Bookbank book = new Bookbank("Name", 0);
        boolean result = book.deposit(100);
        assertEquals(true, result);
        assertEquals(100.0, book.getBalance(), 0.00001);
    }

    @Test
    public void shouldDepositNegative() {
        Bookbank book = new Bookbank("Name", 0);
        boolean result = book.deposit(-100);
        assertEquals(false, result);
        assertEquals(0, book.getBalance(), 0.00001);
    }

    @Test
    public void shouldWithdrawSuccess() {
        Bookbank book = new Bookbank("Name", 0);
        book.deposit(100);
        boolean result = book.withdraw(50);
        assertEquals(true, result);
        assertEquals(50, book.getBalance(), 0.00001);
    }

    @Test
    public void shouldWithdrawNegative() {
        Bookbank book = new Bookbank("Name", 0);
        book.deposit(100);
        boolean result = book.withdraw(-50);
        assertEquals(false, result);
        assertEquals(100, book.getBalance(), 0.00001);
    }

    @Test
    public void shouldWithdrawOverBalance() {
        Bookbank book = new Bookbank("Name", 0);
        book.deposit(50);
        boolean result = book.withdraw(100);
        assertEquals(false, result);
        assertEquals(50, book.getBalance(), 0.00001);
    }

    @Test
    public void shouldWithdraw100Balance100() {
        Bookbank book = new Bookbank("Name", 0);
        book.deposit(100);
        boolean result = book.withdraw(100);
        assertEquals(true, result);
        assertEquals(0, book.getBalance(), 0.00001);
    }

}
