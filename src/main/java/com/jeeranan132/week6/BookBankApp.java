package com.jeeranan132.week6;

public class BookBankApp {
    public static void main(String[] args) {
        Bookbank jeeranan = new Bookbank("Jeeranan", 100.0); // Default Contructor
        jeeranan.print();
        jeeranan.deposit(50);
        jeeranan.print();
        jeeranan.withdraw(50);
        jeeranan.print();

        Bookbank prayood = new Bookbank("Prayoood", 1);
        prayood.deposit(1000000);
        prayood.withdraw(10000000);
        prayood.print();

        Bookbank praweet = new Bookbank("Praweeet", 10);
        praweet.deposit(10000000);
        praweet.withdraw(1000000);
        praweet.print();
    }
}
